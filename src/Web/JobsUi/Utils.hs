module Web.JobsUi.Utils where

lookupIO :: Show a => Eq a => a -> [(a, b)] -> IO b
lookupIO key = maybe (error $ "Missing value for key: " <> show key) pure . lookup key
