{- | Jobs-Ui example

We'll create a job server where a user can run one of the following jobs:

- Create a file in /tmp and write some content into it
- Divide two numbers


-}

module Main where

import qualified Data.Text as T
import qualified Data.Text.IO as T
import Text.Read
import Control.Concurrent

import Web.JobsUi

main :: IO ()
main = run 1337 jobtypes

jobtypes :: [JobType]
jobtypes =
  [ JobType divideJob
  , JobType writeTmpJob
  ]


-- Divide two integers --

data DivideInfo
  = DivideInfo
  { numerator :: Int
  , denominator :: Int
  }

-- description of a divide job
divideJob :: JobInfo DivideInfo
divideJob = JobInfo
  -- This will be the name of this type of jobs
  { jiType = "divide"

  -- Defining two arguments of type TextInput for this job
  , jiInputs =
    [ Param
      -- Parameter description will be used to lookup the value in jiConstructor
      { paramDesc = "Numerator"
      , paramInputType = TextInput
      -- Make sure the input is legal and throw an error if it isn't
      , paramValidation = pure
        . maybe (Error "expecting an integer") (Success . T.pack . show)
        . readMaybe @Int
        . T.unpack
      }
    , Param
      -- Parameter description will be used to lookup the value in jiConstructor
      { paramDesc = "Denominator"
      , paramInputType = TextInput
      , paramValidation = pure
      -- Make sure the input is legal and throw an error if it isn't
        . maybe (Error "expecting an integer") (Success . T.pack . show)
        . readMaybe @Int
        . T.unpack
      }
    ]

  -- Will be used to display the job afterwards
  , jiParams = \DivideInfo{..} ->
      [ T.pack $ show numerator
      , T.pack $ show denominator
      ]

  -- Construct a DivideJob from the user input
  , jiConstructor = \params -> do
    DivideInfo
      <$> (readIO . T.unpack =<< lookupIO "Numerator" params)
      <*> (readIO . T.unpack =<< lookupIO "Denominator" params)

  -- Execute the job, returning the result to be displayed in the website
  , jiExec = \DivideInfo{..} -> do
      threadDelay 5000000
      pure $ T.pack $ show (numerator `div` denominator)

  -- We don't want to be notified when a job is done
  , jiNotify = \_ _ -> pure ()
  }

-- Write file to /tmp --

-- These represents one of the three files we can write to
data File
  = One
  | Two
  | Three
  deriving (Show, Read)

-- Where to write and what
data WriteTmpInfo
  = WriteTmpInfo
  { path :: File
  , content :: T.Text
  }

-- description of a write job
writeTmpJob :: JobInfo WriteTmpInfo
writeTmpJob = JobInfo
  { jiType = "write file"

  , jiInputs =
    [ Param
      { paramDesc = "File"
      , paramInputType = TextOptions $ pure ["One", "Two", "Three"]
      , paramValidation = pure . Success
      }
    , Param
      { paramDesc = "Content"
      , paramInputType = TextInput
      , paramValidation = pure . Success
      }
    ]

  , jiParams = \WriteTmpInfo{..} ->
      [ T.pack $ show path
      , content
      ]

  , jiConstructor = \params -> do
    WriteTmpInfo
      <$> (readIO . T.unpack =<< lookupIO "File" params)
      <*> lookupIO "Content" params

  , jiExec = \WriteTmpInfo{..} -> do
      threadDelay 2000000
      T.writeFile ("/tmp/" <> show path) content
      pure $ "Written content into /tmp/" <> T.pack (show path)

  -- we will write this to stdout when the job is done
  , jiNotify = \_ _ -> putStrLn "finished writing"
  }

