Jobs UI is a UI system for queuing and running custom IO Jobs.

> License: Apache License 2.0

![Example image](view.png)

## Usage

[See the example project](https://gitlab.com/gilmi/jobs-ui/tree/master/example/Main.hs).

Define your own custom jobs using the `JobInfo` type:

```hs
data JobInfo a
  = JobInfo
  { jiType :: T.Text -- ^ The name of the job type.
  , jiInputs :: [Param] -- ^ Definition of user parameters.
  , jiParams :: a -> [T.Text]
    -- ^ Conversion from the job payload structure to presentable parameters.
  , jiConstructor :: [(T.Text, T.Text)] -> IO a
    -- ^ Construct a payload type from user entered parameters.
  , jiExec :: a -> IO T.Text
    -- ^ How to execute a job. Returns a presentable Html when the job succeeds.
  , jiNotify :: a -> (Result T.Text T.Text) -> IO ()
    -- ^ Will be run when job ends.
  }
```

Wrap it in the `JobType` constructor, and pass it to the `run :: [JobType] -> IO ()`.
