.PHONY: setup

setup:
	stack setup

.PHONY: build

build:
	stack build

.PHONY: dev

dev:
	stack build --fast --file-watch

.PHONY: clean_all

clean:
	stack clean
